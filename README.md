# RPi installation

Provide SD-card image for out-of-the-box setup of jupyter notebook server,
berryconda, python-seabreeze, pyserial, GPIO,
... and a git connection for "firmware updates".

The installation process is detailed in the following order:
- [Creation of custom OS image](#createImage)
- [Usage of custom OS image](#usingImage)


---
## <a name="createImage"></a>Create modified OS image:

1. * [ ] Download latest **Raspbian Stretch Lite** from [here](https://www.raspberrypi.org/downloads/raspbian/) or fall back to [this file](/2018-11-13-raspbian-stretch-lite.zip)
and flash image to SD-card. On linux or mac OS, use *restore image* option using a graphical tool and see [this guide](https://howtoraspberrypi.com/how-to-raspberry-pi-headless-setup/) for further instructions.
Mount boot partition and create an empty file named *ssh* in order to enable ssh access. If supported by the RPi, copy a *wpa_supplicant.conf* file like [this](/wpa_supplicant.conf) containing network configuration to the boot partition to activate automatic connection to WIFI (or hotspot). Create a hashed passphrase in a terminal using `wpa_passphrase YOUR_SSID YOUR_PASSWORD` before hardcoding your password in plain text in the `wpa_supplicant.conf` file.   


2. * [ ] Insert SD-card in RPi, boot and create a new user and password, then remove user `pi`:
    - log in as `pi` (default password is "raspberry") via ssh
    - run `sudo raspi-config` to set location settings and hostname = "pydevctrl"
    - reboot
    - add new user "tna" and password by running `sudo adduser tna`
    - add tna to the following groups: adm, dialout, sudo, plugdev, audio, video, users, netdev, input, spi, ic2, gpio by running `sudo adduser tna GROUP_NAME` for each GROUP_NAME
    - type `sudo visudo` and add/change line `tna ALL=(ALL) NOPASSWD: ALL` to give `tna` root privileges (and the no passwd option to allow sudo commands pulled from remote later on)
    - log in as the new user `tna` and delete user `pi` by `sudo userdel -r pi`


3. * [ ] With internet connection, install git: `sudo apt-get install git`, then
    - `git config --global user.email "info@tnils.eu"`
    - `git config --global user.name "TobiasNils"`


4. * [ ] Link to GitLab
    - copy ssh key pair from [/.ssh/](/.ssh/) to `~/.ssh/` on rpi, *or*:
    - create new ssh key-pair as detailed [here](https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair) to connect to [GitLab repository](https://gitlab.com/TobiasNils)
    - clone [pydevctrl-firmware](git@gitlab.com:python-for-device-control/pydevctrl-firmware.git) and [database](git@gitlab.com:python-for-device-control/database.git).
    - test that everything works by creating a file in one of the projects and pushing the committed changes


3. * [ ] **Berryconda** (Anaconda Python for Raspberry Pi):
    - download installer from [here](https://github.com/jjhelmus/berryconda) using (on rpi)

    ```
    wget https://github.com/jjhelmus/berryconda/releases/download/v2.0.0/Berryconda3-2.0.0-Linux-armv7l.sh
    chmod a+rx Berryconda3-VERSION-Linux-armv7l.sh
    ./Berryconda3-VERSION-Linux-armv7l.sh
    ```
    - install to `~/.opt/berryconda3`
    - *do add* conda python to PATH variable when asked
    - Install additional python packages in a new virtual environment using [requirements.yml](requirements.yml)
    with ```conda env create -f pydevctrl-firmware/modules/requirements.yml```

7. - [ ] Init aliases file and add jupyter command pointing to newly created conda environment: `echo  "alias jupyter=/home/tna/.opt/berryconda3/envs/pydevctrl/bin/jupyter" > .bash_aliases`

8. - [ ] copy [jupyter_notebook_config.py](jupyter_notebook_config.py) to `~/.jupyter/`. Password is set to "pydevctrl"

5. * [ ] Install udev-rules for **Ocean-Optics** spectrometers:
    - copy [10-oceanoptics.rules](10-oceanoptics.rules) to `/etc/udev/rules.d/`
    - run `sudo udevadm control --reload-rules`

6. * [ ] Install services in pydevctrl-firmware/.services

7. * [ ] set self maintenance of journalctl by size restriction: `journalctl --vacuum-size=100M`

8. * [ ] install __DWservice__ as client application using installation number `107-393-409`:

    ```
    sudo wget https://www.dwservice.net/download/dwagent_generic.sh && sudo sh dwagent_generic.sh
    ```

8. * [ ] __Create disc image for re-use__

## <a name="usingImage"></a>Deploy modified OS image for the first time

1. * [ ] flash/restore image to new SD-card
2. * [ ] expand partition to fill SD-card
3. * [ ] insert into RPi and login as user `tna` via __ssh__ (be connected to the same local LAN as the RPi and type `ssh tna@pydevctrl` in a terminal on your PC)
4. * [ ] set user password: type `passwd` in terminal and follow instructions (old passwd is `pydevctrl`)
2. * [ ] navigate to `database` git project directory and create new "client" branch with a experiment specific name, then push this branch upstream :
```
cd database
git pull
git checkout -b 'example-client-branch'
git push -u origin 'example-client-branch'
```
Ready to go.

## Start an experiment
1. Connect via `ssh`
2. Type `jupyter notebook`, then on your PC, the "host", navigate in a browser to http://pydevctrl:8888 and log in using password "`pydevctrl`".
3. Launch __Acquisition - Init.ipynb__, run the cells from the top down, adjust settings while you are at it and hit `M.start_repetition()`

During this last command, a file named `acquisition.params` is created in the directory `database/data/EXPERIMENT_NAME` containing a parameter `stop`, which is set `false`.

You can now safely disconnect from _Jupyter_ as well as the _ssh_ shell.

Data acquisition will restart automatically as long as `stop = false`.
